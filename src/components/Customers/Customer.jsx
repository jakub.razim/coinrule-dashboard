import React from "react";

export const Customer = (props) => {

    const badgeClass = props.data.activity === "Active" ? "success" : "danger";
    const exchanges = props.data.exchanges.map((ex, ix) => {
        if(ix < props.data.exchanges.length-1) return ex + ", ";
        return ex;
    });

    return(
        <React.Fragment>
            <tr>
                <td>
                    <img src={props.data.image} alt={props.data.firstname + " " + props.data.surname} className="rounded-circle mr-1" width="25"/>
                    {props.data.firstname} {props.data.surname}
                </td>
                <td>{props.data.email}</td>
                <td>{props.data.countryCode}</td>
                <td>
                    {exchanges}
                </td>
                <td><span className={`badge badge-${badgeClass}`}>{props.data.activity}</span></td>
                <td>{props.data.plan.name}</td>
                <td>
                    <button className="btn btn-sm p-0">
                        <i className="mdi mdi-square-edit-outline"/>
                    </button>
                    <button className="btn btn-sm p-0 pl-1">
                        <i className="mdi mdi-delete"/>
                    </button>
                </td>
            </tr>
        </React.Fragment>
    );
};