import React, {Component} from "react";
import {Customer} from "./Customer";
import {Card, CardBody, Col, Row} from "reactstrap";
import {Users} from "./dummy-data/data";
import Loader from "../Loader";

export default class Customers extends Component {
    constructor(props) {
        super(props);

        this.state = {
            allChecked: false,
            users: Users.data,
            bulletClass: null,
            nameArrow: "↓"
        }

    }

    filterStatus = () => {
        if (this.state.bulletClass === null) {
            this.setState({
                bulletClass: "text-success"
            })
        } else if (this.state.bulletClass === "text-success") {
            this.setState({
                bulletClass: "text-danger"
            })
        } else {
            this.setState({
                bulletClass: null
            })
        }
    };

    sortByAlphabet = () => {

        this.setState({
            nameArrow: this.state.nameArrow === "↓" ? "↑" : "↓"
        });
    };

    handleSearch = (e) => {
        this.setState({
            users: Users.data.filter(user => {
                const stick = (user.firstname + " " + user.surname).toLowerCase();
                if(stick.includes(e.target.value.toLowerCase())){
                    return stick;
                }
            })
        })
    };

    componentDidMount() {
        this.setState({
            users: Users.data.sort(function(a, b){
                if(a.surname < b.surname) { return -1; }
                if(a.surname > b.surname) { return 1; }
                return 0;
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {

        if (prevState.bulletClass !== this.state.bulletClass) {

            if (this.state.bulletClass === null) {
                this.setState({
                    users: Users.data
                })
            } else if (this.state.bulletClass === "text-success") {
                this.setState({
                    users: Users.data.filter(user => user.activity === "Active")
                })
            } else if (this.state.bulletClass === "text-danger") {
                this.setState({
                    users: Users.data.filter(user => user.activity === "Inactive")
                })
            }
        }

        if(prevState.nameArrow !== this.state.nameArrow) {
            if(this.state.nameArrow === "↓"){
                this.setState({
                    users: Users.data.sort(function(a, b){
                        if(a.surname < b.surname) { return -1; }
                        if(a.surname > b.surname) { return 1; }
                        return 0;
                    })
                })
            } else {
                this.setState({
                    users: Users.data.sort(function(a, b){
                        if(a.surname > b.surname) { return -1; }
                        if(a.surname < b.surname) { return 1; }
                        return 0;
                    })
                })
            }


        }

    }


    render() {

        // Error, nepodařilo se autorizovat

        // fetch("http://localhost/users", {
        //     method: 'GET'
        // })
        //     .then(response => response.json())
        //     .then(json => console.log(json));

        return (
            <React.Fragment>

                {this.props.loading && <Loader />}

                <Row>
                    <Col>
                        <div className="page-title-box">
                            <Row>
                                <Col lg={7}>
                                    <h4 className="page-title">Users</h4>
                                </Col>
                                <Col lg={5} className="mt-lg-3 mt-md-0">

                                </Col>
                            </Row>
                        </div>
                    </Col>
                </Row>

                <Row>
                    <Col lg={12}>
                        <Card>
                            <CardBody>
                                <label htmlFor="searchName">Search by name</label>
                                <input id="searchName" className="input-group-text mb-2" type="text" onChange={this.handleSearch}/>
                                <table className="table table-dark">
                                    <thead>
                                    <tr>
                                        <th scope="col">
                                            <button className="btn p-0" onClick={this.sortByAlphabet}>
                                                Name {this.state.nameArrow}
                                            </button>
                                        </th>
                                        <th scope="col">Email</th>
                                        <th scope="col">Country code</th>
                                        <th scope="col">Exchanges</th>
                                        <th scope="col">
                                            <button className="btn p-0" onClick={this.filterStatus}>
                                                <span className={this.state.bulletClass}>Status</span>
                                            </button>
                                        </th>
                                        <th scope="col">Plan</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {
                                        this.state.users.map(user => <Customer key={user.id} data={user} />)
                                    }
                                    </tbody>
                                </table>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </React.Fragment>


        );
    }

}