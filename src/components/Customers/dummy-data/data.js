

export const Users = {
    data: [
        {
            id: '00001',
            firstname: 'Petr',
            surname: 'Novák',
            image: 'https://coderthemes.com/ubold/layouts/dark/assets/images/users/user-4.jpg',
            email: 'pn1@mailinator.com',
            countryCode: 'CZ',
            exchanges: [
                'Demo Exchange',
                'Binance'
            ],
            activity: 'Active',
            plan: {
                uid: 'starter-trial',
                name: 'Starter'
            }
        },
        {
            id: '00002',
            firstname: 'Pavel',
            surname: 'Novotný',
            image: 'https://coderthemes.com/ubold/layouts/dark/assets/images/users/user-3.jpg',
            email: 'pn2@mailinator.com',
            countryCode: 'CZ',
            exchanges: [
                'Demo Exchange'
            ],
            activity: 'Inactive',
            plan: {
                uid: 'disabled',
                name: 'Limited'
            }
        },
        {
            id: '00003',
            firstname: 'Barbora',
            surname: 'Světlá',
            image: 'https://coderthemes.com/ubold/layouts/dark/assets/images/users/user-1.jpg',
            email: 'bs1@mailinator.com',
            countryCode: 'CZ',
            exchanges: [
                'Demo Exchange',
                'Binance'
            ],
            activity: 'Active',
            plan: {
                uid: 'free',
                name: 'Free'
            }
        },
        {
            id: '00004',
            firstname: 'Lucie',
            surname: 'Výborná',
            image: 'https://coderthemes.com/ubold/layouts/dark/assets/images/users/user-8.jpg',
            email: 'lv1@mailinator.com',
            countryCode: 'CZ',
            exchanges: [
                'Demo Exchange',
                'Binance'
            ],
            activity: 'Active',
            plan: {
                uid: 'starter',
                name: 'Starter'
            }
        },
        {
            id: '00005',
            firstname: 'Michal',
            surname: 'Černý',
            image: 'https://coderthemes.com/ubold/layouts/dark/assets/images/users/user-6.jpg',
            email: 'mc1@mailinator.com',
            countryCode: 'CZ',
            exchanges: [
                'Demo Exchange'
            ],
            activity: 'Inactive',
            plan: {
                uid: 'free',
                name: 'Free'
            }
        }
    ],
    totalPages: 1
};